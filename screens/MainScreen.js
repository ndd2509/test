/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import {
  View,
  FlatList,
  Image,
  Dimensions,
  Text,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Star from '../assets/svg/star.svg';
import Clock from '../assets/svg/clock.svg';
import Marker from '../assets/svg/marker.svg';
const food1 = require('../assets/images/foodImages/food1.png');
const windowWidth = Dimensions.get('window').width;
const itemWidth = windowWidth - 60;
const burgers = require('../assets/images/foodImages/burgers.png');
const pizza = require('../assets/images/foodImages/pizza.png');
const bbq = require('../assets/images/foodImages/bbq.png');
const fruit = require('../asseruit.png');
const sushi = require('../assets/images/foodImages/sts/images/foodImages/fushi.png');
const noodle = require('../assets/images/foodImages/noodle.png');
function ItemRestaurant({dataItem}) {
  return (
    <View
      style={{
        flexDirection: 'row',
        width: itemWidth,
        marginBottom: 10
      }}>
      <Image style={{ width: itemWidth / 3, height: itemWidth / 3 }}
        source={dataItem.img}
      />
      <View style={{ marginLeft: 20, flexDirection: 'column' }}>
        <Text style={{ fontSize: 14, marginBottom: 10, fontWeight: '700' }}>{dataItem.name}</Text>
        <Text style={{ color: '#34495E', marginBottom: 10, fontSize: 12, maxWidth: '80%' }}><Marker /> {dataItem.address}</Text>
        <Text style={{ color: '#34495E', marginBottom: 10, fontSize: 12 }}><Clock /> {dataItem.distance}</Text>
        <View style={{ flexDirection: 'row' }}>{Array.from(Array(dataItem.star)).map((item, index) => <Star
          key={index}
        />)}</View>
      </View>
    </View>
  );
}

export default function MainScreen({changeScreen}) {
  const [listNearMe, setListNearMe] = useState([
    {
      id: 1,
      food: 'Burgers',
      img: burgers,
    },
    {
      id: 2,
      food: 'Pizza',
      img: pizza,
    },
    {
      id: 3,
      food: 'BBQ',
      img: bbq,
    },
    {
      id: 4,
      food: 'Fruit',
      img: fruit,
    },
    {
      id: 5,
      food: 'Sushi',
      img: sushi,
    },
    {
      id: 6,
      food: 'Noodle',
      img: noodle,
    },
  ]);

  const renderItem = ({item, index}) => {
    return <ItemRestaurant dataItem={item} />;
  };

  return (
    <View style={{flex: 1, paddingHorizontal: 30}}>
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <TouchableOpacity
          style={{}}
          onPress={() => {
            changeScreen('FoodMenu');
          }}>
          <Image
            style={{width: 50, height: 50}}
            source={require('../assets/images/back.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setListNearMe(listOld => {
              return [
                ...listOld,
                {
                  id: listOld.length + 1,
                  food: 'Something',
                  img: food1,
                
                },
              ];
            });
          }}>
          <Image
            style={{width: 50, height: 50}}
            source={require('../assets/images/add.png')}
          />
        </TouchableOpacity>
      </View>
      <FlatList
        keyExtractor={(item, index) => item.id.toString()}
        data={listNearMe}
        renderItem={renderItem}
      />
      
    </View>
  );
}
