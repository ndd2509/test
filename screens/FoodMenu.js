/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import {View, FlatList, Image, Dimensions, Text} from 'react-native';
import React, {useState} from 'react';

const burgers = require('../assets/images/foodImages/burgers.png');
const pizza = require('../assets/images/foodImages/pizza.png');
const bbq = require('../assets/images/foodImages/bbq.png');
const fruit = require('../assets/images/foodImages/fruit.png');
const sushi = require('../assets/images/foodImages/sushi.png');
const noodle = require('../assets/images/foodImages/noodle.png');

const windowWidth = Dimensions.get('window').width;
const itemWidth = windowWidth - 60;
function ItemRestaurant({dataItem}) {
  return (
    <View
      style={{
        width: itemWidth / 2.5,
        marginVertical: 10,
        marginHorizontal: 5,
        backgroundColor: dataItem.id % 2 === 0 ? '#e1cde9' : '#c2e0f4',
        flexDirection: 'column',
        height: itemWidth / 2.5,
        borderRadius: 20
      }}>
      <Text
        style={{
          marginTop: 10,
          marginLeft: 10,
          fontSize: 14,
          fontWeight: '700',
          color: '#fff',
        }}>
        {dataItem.food}
      </Text>
      <Image
        style={{
          width: itemWidth / 2.5,
          height: itemWidth / 3,
          transform: [{translateX: 0}, {translateY: -15}],
          borderRadius: 20,
        }}
        source={dataItem.img}
      />
    </View>
  );
}

export default function FoodMenu({changeScreen}) {
  const [username, setusername] = useState('');
  const [listNearMe, setListNearMe] = useState([
    {
      id: 1,
      food: 'Burgers',
      img: burgers,
    },
    {
      id: 2,
      food: 'Pizza',
      img: pizza,
    },
    {
      id: 3,
      food: 'BBQ',
      img: bbq,
    },
    {
      id: 4,
      food: 'Fruit',
      img: fruit,
    },
    {
      id: 5,
      food: 'Sushi',
      img: sushi,
    },
    {
      id: 6,
      food: 'Noodle',
      img: noodle,
    },
  ]);

  const renderItem = ({item, index}) => {
    return <ItemRestaurant dataItem={item} />;
  };

  return (
    <View style={{flex: 1, paddingHorizontal: 10}}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 30,
        }}>
        <Text style={{fontSize: 18, fontWeight: '700', color: 'black'}}>
          Food Menu
        </Text>

        <Text
          onPress={() => changeScreen('MainScreen')}
          style={{fontSize: 12, color: 'black'}}>
          View All
        </Text>
      </View>

      <FlatList
        style={{flexWrap: 'wrap'}}
        numColumns={3}
        keyExtractor={(item, index) => item.id.toString()}
        data={listNearMe}
        renderItem={renderItem}
      />
    </View>
  );
}
