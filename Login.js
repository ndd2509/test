/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {StatusBar} from 'react-native';
import {
  // AsyncStorage,
  TouchableOpacity,
  Text,
  View,
  TextInput,
  StyleSheet,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

class AsyncStorageExample extends Component {
  state = {
    name: '',
    savedData: '',
  };
  componentDidMount = () =>
    AsyncStorage.getItem('name').then(value =>
      this.setState({savedData: value}),
    );

  setData = value => {
    AsyncStorage.setItem('name', JSON.stringify([{id: 1, name: value}]));
  };
  getData = value => {
    AsyncStorage.getItem('name').then(value =>
      this.setState({savedData: value}),
    );
    console.log(this.state.name);
  };
// console.log(name);
  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.textInput}
          autoCapitalize="none"
          value={this.state.value}
          onChangeText={value => {
            this.setData(value);
          }}
        />
        <Text>{this.state.savedData}</Text>
        <TouchableOpacity
          activeOpacity={0.5}
          onPress={() => {
            this.setData(this.state.name);
          }}>
          <View style={styles.saveButton}>
            <Text style={styles.textStyle}>{'Save Data'}</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.5}
          onPress={() => {
            this.getData(this.state.name);
          }}>
          <View style={styles.saveButton}>
            <Text style={styles.textStyle}>{'Get Data'}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
export default AsyncStorageExample;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginTop: 50,
  },
  textStyle: {
    color: 'white',
    textAlign: 'center',
  },
  saveButton: {
    width: 100,
    height: 40,
    marginBottom: 10,
    marginTop: 15,
    borderRadius: 8,
    borderColor: 'white',
    justifyContent: 'center',
    borderWidth: 1,
    color: 'white',
    backgroundColor: '#7685ed',
  },
  textInput: {
    width: '80%',
    height: 100,
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#7685ed',
  },
});
