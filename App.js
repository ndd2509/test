import React, {useState, useEffect} from 'react';
import {View, SafeAreaView} from 'react-native';

import FlexBox from './screens/FlexBox';
import LoginScreen from './screens/LoginScreen';
import MainScreen from './screens/MainScreen';
import SignUp from './screens/SignUp';
import AsyncStorage from '@react-native-async-storage/async-storage';
import FoodMenu from './screens/FoodMenu';
export default function App() {
  const [checkState, setState] = useState('FoodMenu');
  useEffect(() => {
    AsyncStorage.getItem('user').then(user => user && setState('FoodMenu'));
  }, []);

  // const listAccount = [
  //   {
  //     username: 'hello1234',
  //     password: '123456',
  //   }
  // ]
  // AsyncStorage.setItem('account', JSON.stringify(listAccount));

  const changeScreen = screenName => {
    setState(screenName);
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 1}}>
        {checkState == 'MainScreen' ? (
          <MainScreen changeScreen={changeScreen} />
        ) : checkState == 'LoginScreen' ? (
          <LoginScreen changeScreen={changeScreen} />
        ) : checkState == 'SignUp' ? (
          <SignUp changeScreen={changeScreen} />
        ) : checkState == 'FoodMenu' ? (
          <FoodMenu changeScreen={changeScreen} />
        ) : null}
      </View>
    </SafeAreaView>
  );
}
